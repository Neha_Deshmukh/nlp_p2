import parse_train 
import parse_test
import parseDict
import fv

class WordSense():
    senseProb = {}

    def __init__(self):
      
       train = parse_train.TrainParser()
       test = parse_test.TestParser()
       trainingData = train.trainingData
       testData = test.testData 
       tf = fv.FV(trainingData)
       self.trainFeatures = tf.getTrainVectors()
       self.testFeatures = tf.getTestVectors(testData)
       self.senseProb = self.getPY() 

    def getPY(self): 
        senseP = {} 
        for lex,tups in self.trainFeatures.iteritems():
            senses = {}
            listSenses = [] 
            totalSenses = 0 
            for tup in tups: 
                totalSenses += len(tup[1])
                for sense in tup[1]:
                      if sense in senses: 
                        senses[sense] += 1
                      else :
                         senses[sense] = 1
            for sense in senses:
                 listSenses.append(sense)
                 senses[sense] += 1
                 totalSenses += 1
            self.trainFeatures[lex].append(([1]*len(tups[0][0]),listSenses))

            for sense,count in senses.iteritems():
                senses[sense] = (count , (float(count) / totalSenses))
            senseP[lex] = senses
        return senseP


    def getPXY(self):
        allPXY = {}
        for lex,tups in self.trainFeatures.iteritems():
            #tups are vector, senselist pairs
            PXS = [{j:0 for j in self.senseProb[lex]} for i in range(len(tups[0][0]))]#first feature vector length
            for tup in tups:
                senses = tup[1]
                for index,feature in enumerate(tup[0]):
                    for sense in senses:
                        PXS[index][sense]+=feature
            allPXY[lex] = PXS
        return allPXY
         

    def getPYX(self):
        PXY = self.getPXY()
        f = open("kagglePreds.txt","w")
        for lex, tups in self.testFeatures.iteritems():
            senses = self.senseProb[lex]
            for instance in tups:
                senseProbs = {sense:1 for sense in self.senseProb[lex]}
                for sense in senses:
                    senseProbs[sense] *= self.senseProb[lex][sense][0]
                    for index, feature in enumerate(instance[0]):
                        if feature==1:
                            senseProbs[sense] *= (PXY[lex][index][sense])/float(self.senseProb[lex][sense][0])
                        else:
                            senseProbs[sense] *= (1.0 - (PXY[lex][index][sense])/float(self.senseProb[lex][sense][0]))
                bestSense = max(senseProbs.iterkeys(), key=(lambda key: senseProbs[key])) 
                pred = instance[2]+","+bestSense+"\n"
                f.write(pred)
                













