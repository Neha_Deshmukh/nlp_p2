import nltk 
#Make this bigger and see if accuracy improves?
TOP =20 

class FV():
    def __init__(self,data):
        """data is of a dictionary of the following form:
        {lexelt(string): [
                          ( 
                            context(string), 
                            head(string),
                            [sense(string)]
                          )
                         ]
        }

        We'll be converting this to feature vectors for each lexelt.
        FV: {lexelt(string): [(vector(int list), [sense(string)])]}
        """
        self.vectors = {lex:[] for lex in data} 
        self.frequent_words = {lex:[] for lex in data}
        count = 1 
        for lexelt,values in data.iteritems():
            word_bag = ''
            #print lexelt
            for instance in values:#values is a list of 3-tuple
                word_bag += instance[0]
            #maybe stem here for better performance?

            text = nltk.word_tokenize(word_bag)
            text = nltk.pos_tag(text)
            #in the line below, add extra conditions to the ifs, using or
            #like, pair[1][0] = "V". , or other parts of speech
            text = [pair[0] for pair in text if pair[1][0:2]=='NN']
            distribution = nltk.FreqDist(text).most_common()
            min_value = distribution[TOP][1]
    
            freq_words = [i[0] for i in distribution if i[1]>=min_value]
            self.frequent_words[lexelt] = freq_words
            for instance in values:
                vect = [int(word in instance[0]) for word in freq_words]
                self.vectors[lexelt].append((vect, instance[1]))


        def getTrainVectors(self):
            return self.vectors

        def getTestVectors(self,data):
            vectors = {lex:[] for lex in self.vectors} 
            for lexelt,values in data.iteritems():
                words = self.frequent_words[lexelt]
                for instance in values:
                    vect = [int(word in instance[0]) for word in words]
                    vectors[lexelt].append((vect, instance[1]))

        #At this point vectors is valid.
                
