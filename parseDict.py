from bs4 import BeautifulSoup 

class DictParser(): 
    
    Dictionary = {}

    def __init__(self):

        handler = open("Dictionary.xml").read()
        soup = BeautifulSoup(handler,"xml")
        lexelts = soup.find_all("lexelt")
        for lex in lexelts:
            senses = self.parseLexelt(lex)
            self.Dictionary[lex['item']] = senses

    def parseLexelt(self, lex):
        instances = list(lex.find_all("sense"))
        while True:
            try:
                instances.remove("\n")
            except ValueError:
                 break
        senses = []
        for instance in instances :
            senses.append(instance["id"])
        return senses

