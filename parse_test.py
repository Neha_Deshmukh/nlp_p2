from bs4 import BeautifulSoup 

class TestParser(): 
    
    testData = {}

    def __init__(self):

        handler = open("test-data.data").read()
        soup = BeautifulSoup(handler,"xml")
        lexelts = soup.find_all("lexelt")
        for lex in lexelts:
            lexTup = self.parseLexelt(lex)
            self.testData[lex['item']] = lexTup

    def parseLexelt(self, lex):
        instances = list(lex.children)
        while True:
            try:
                instances.remove("\n")
            except ValueError:
                 break
        lexeltTuples = []
        for instance in instances :
            cont = instance.context.text
            form = instance.head.text
            Id = instance["id"]
            tup = (cont,form,Id)
            lexeltTuples.append(tup)
        return lexeltTuples