from bs4 import BeautifulSoup 

class TrainParser(): 
    
    trainingData = {}

    def __init__(self):

        handler = open("training-data.data").read()
        soup = BeautifulSoup(handler,"xml")
        lexelts = soup.find_all("lexelt")
        for lex in lexelts:
            lexTup = self.parseLexelt(lex)
            self.trainingData[lex['item']] = lexTup

    def parseLexelt(self, lex):
        instances = list(lex.children)
        while True:
            try:
                instances.remove("\n")
            except ValueError:
                 break
        lexeltTuples = []
        for instance in instances :
            cont = instance.context.text
            form = instance.head.text
            answers = instance.find_all('answer')
            senses = []
            for answer in answers: 
                senses.append(answer['senseid'])
            tup = (cont,senses,form)
            lexeltTuples.append(tup)
        return lexeltTuples

